pragma solidity 0.5.8;
//pragma experimental ABIEncoderV2;

contract government_contract{
    //users have first name , last name , aadhar no, gender
    struct user {
        string f_name;
        string l_name;
        string aadhar_no;
        address payable user_address;
        bool registered;
    }
    
    //admins have name , aadhar no
    struct admin {
        string f_name;
        string l_name;
        uint256 admin_id;
        address payable admin_address;
    }
    
    //subsidies have name , deatils , amount
    struct subsidie {
        string name;
        string details;
        uint amount;
        uint id;
    }
    
    //applications have subsidie id , user address, application status
    struct application{
        uint subsidie_id;
        address payable user_address;
        string status;
    }
    
    //initialize users, applications, admins , subsidies
    address public owner_address;
    uint public no_users;
    uint public no_applications;
    uint public no_subsidies;
    uint public no_admins;
    address[] public user_addresses;
    address payable[] public admin_addresses ;
    
    //mappings
    mapping(address => user) public users;
    mapping(address => admin) public admins;
    mapping(uint => subsidie) public subsisdies;
    mapping(uint => application) public applications;
    
    
    //initialize the contract
    constructor() public{
        owner_address = msg.sender;
        no_users = 0;
        no_admins = 0;
        no_applications = 0;
        no_subsidies = 0;
        no_users =- 0;
    }
    
    //helper function to compare strings
    function hashCompareWithLengthCheck(string memory a, string memory b) internal pure returns (bool) {
        if(bytes(a).length != bytes(b).length) {
            return false;
        }
        else {
        return keccak256(abi.encodePacked(a)) == keccak256(abi.encodePacked(b));
        }
    }
    
    //helper function to check if subsidie of same name check_subside_exists
    function check_subside_exists(string memory _name) internal view returns (bool){
        for(uint i = 0; i <= no_subsidies ; i++){
            if(hashCompareWithLengthCheck(_name,subsisdies[i].name)){
                return false;
            }
        }
        return true;
    }
    
    //check if sender is admin
    function sender_is_admin(address adr) internal view returns(bool){
        for(uint i = 0 ; i <= no_admins ; i++){
            if(admin_addresses[i] == adr){
                return true;
            }
        }
        return false;
    }
    
    //check if application already there
    function check_application_exists(address adr, uint _subsidie_id) internal view returns(bool){
        for(uint i = 0 ; i <= no_applications ; i++){
            if(applications[i].subsidie_id ==  _subsidie_id && applications[i].user_address == adr){
                return true;
            }
        }
        return false;
    }
    
    //accept user details and push to users array
    function create_user(string memory _f_name, string memory _l_name, string memory _aadhar_no) public {
        address payable _address = msg.sender;
        require(!users[_address].registered);
        users[_address].f_name = _f_name;
        users[_address].l_name = _l_name;
        users[_address].aadhar_no = _aadhar_no;
        users[_address].user_address = _address;
        users[_address].registered = true;
        user_addresses.push(_address);
        no_users += 1; 
    }
    
    //accept name description and amount and push to subsisdies array
    function create_subsidies(string memory _name, string memory _details, uint _amount) public {
        //require(check_subside_exists(_name));
        require(msg.sender == owner_address);
        subsisdies[no_subsidies].name  = _name;
        subsisdies[no_subsidies].details = _details;
        subsisdies[no_subsidies].amount = _amount;
        subsisdies[no_subsidies].id = no_subsidies + 1;
        no_subsidies += 1;
    }
    
    //accept user and subsidie and push to applications array
    function create_application(uint _subsidie_id ) public {
        require(!check_application_exists(msg.sender,_subsidie_id));
        require(users[msg.sender].registered);
        applications[no_applications].subsidie_id = _subsidie_id;
        applications[no_applications].user_address = msg.sender;
        applications[no_applications].status = "created";
        no_applications += 1;
    }
    
    //change application status to accepted
    function accept_application(uint _application_id)public{
        require(_application_id < no_applications);
        require(hashCompareWithLengthCheck(applications[_application_id].status,"created"));
        applications[_application_id].status = "accepted";
    }
    
    //change application status to rejected
    function reject_application(uint _application_id)public{
        require(_application_id < no_applications);
        require(hashCompareWithLengthCheck(applications[_application_id].status , "created"));
        applications[_application_id].status = "rejected";
    }
    
    //change application status to completed
    function completed_application(uint _application_id)public payable{
        require(_application_id < no_applications);
        require(hashCompareWithLengthCheck(applications[_application_id].status , "accepted"));
        applications[_application_id].user_address.transfer(subsisdies[applications[_application_id].subsidie_id].amount);
    }
    
    
    
    //transfer funds from owner account to admin account
    function transfer_fund_to_admins(uint _deposit) public payable {
       for(uint i = 0; i < no_admins ; i++){
           admin_addresses[i].transfer(_deposit);
       } 
    }
    
    //return a user
    function return_user_from_add(address  adr) public view returns (string memory, string memory, address) {
        return (users[adr].f_name,users[adr].l_name,adr);
    }
    
    function return_user_from_array(uint i) public view returns(string memory, string memory, address){
        return return_user_from_add(user_addresses[i]);
    }
    
    //return a admin
    function return_admin_from_add(address  adr) public view returns (string memory, string memory, address) {
        return (admins[adr].f_name,admins[adr].l_name,adr);
    }
    
    function return_admin_from_array(uint i) public view returns(string memory, string memory, address){
        return return_admin_from_add(admin_addresses[i]);
    }
    
    //return a application
    function return_application_from_array(uint i) public view returns(uint, address ,string memory){
        return (applications[i].subsidie_id,applications[i].user_address, applications[i].status);
    }
    
    //add admin
    function add_admin(address payable _address, string memory _f_name, string memory _l_name) public {
        require(msg.sender == owner_address);
        admins[_address].admin_address = _address;
        admins[_address].f_name = _f_name;
        admins[_address].l_name = _l_name;
        admins[_address].admin_id = no_admins;
        no_admins += 1;
    }
    
    function return_subsidies_from_array(uint i) public view returns(string memory, string memory, uint, uint){
        return (subsisdies[i].name,subsisdies[i].details,subsisdies[i].amount,subsisdies[i].id);
    }
    
    
}