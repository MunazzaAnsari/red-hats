import React, { Component } from "react";
import "./App.css";
import Web3 from "web3";
import Government from "../abis/Government.json";
import { Link } from "react-router-dom";
class Sub {
	constructor(name, details, amount, id) {
		this.name = name;
		this.details = details;
		this.amount = amount;
		this.id = id;
	}
}
class subsidies extends Component {
	constructor(props) {
		super(props);
		this.state = {
			account: "",
			productCount: 0,
			oo: 0,
			products: [],
			loading: true,
			subsidiesa: []
		};
	}
	async componentWillMount() {
		this.loadWeb3();
		this.loadBlockchainData();
		// this.loadss();
	}

	async loadWeb3() {
		if (window.ethereum) {
			window.web3 = new Web3(window.ethereum);
			await window.ethereum.enable();
		} else if (window.web3) {
			window.web3 = new Web3(window.web3.currentProvider);
		} else {
			window.alert(
				"Non-Ethereum browser detected. You should consider trying MetaMask!"
			);
		}
	}

	async loadBlockchainData() {
		const web3 = window.web3;
		const accounts = await web3.eth.getAccounts();
		//console.log(accounts);
		this.setState({ account: accounts[0] });
		const networkId = await web3.eth.net.getId();
		const networkData = Government.networks[networkId];
		if (networkData) {
			//console.log(Government.abi);
			const government = new web3.eth.Contract(
				Government.abi,
				networkData.address
			);
			// 			let owner = await government.methods.owner_address().call();
			// 			//console.log(owner);
			let no_of_subsidies = await government.methods.no_subsidies().call();
			no_of_subsidies = no_of_subsidies.toString();
			no_of_subsidies = parseInt(no_of_subsidies);
			// 			//console.log(no_of_subsidies);
			this.setState({ no_of_subsidies });
			// 			// Load products
			let subsidiesa = [];

			for (var i = 0; i < this.state.no_of_subsidies; i++) {
				const subsidy = await government.methods.subsisdies(i).call();
				let _id = subsidy[2].toString();
				console.log(_id);
				console.log(typeof _id);
				let _sub = new Sub(subsidy[0], subsidy[1], subsidy[2], subsidy[3]);
				// 				//console.log(_sub);
				subsidiesa.push(_sub);
				// this.setState({ subsidiesa });
			}
			this.setState({ subsidiesa });
		} else {
			window.alert("Marketplace contract not deployed to detected network.");
		}
	}

	renderTable() {
		let table = [];
		let header = [
			<td>
				<b>Name</b>
			</td>,
			<td>
				<b>Details</b>
			</td>,
			<td>
				<b>Apply</b>
			</td>
		];
		table.push(header);

		// Outer loop to create parent
		for (let i = 0; i < this.state.no_of_subsidies; i++) {
			let children = [];
			//Inner loop to create children
			for (let j = 0; j < 4; j++) {
				if (j == 0) {
					children.push(<td> {this.state.subsidiesa[i].name}</td>);
				}
				if (j == 1) {
					children.push(<td>{this.state.subsidiesa[i].details}</td>);
				}
				if (j == 2) {
					children.push(
						<td>
							<Link to="./application/:id">
								<button>Click To Apply</button>
							</Link>
						</td>
					);
				}
			}
			//Create the parent and add the children
			table.push(<tr>{children}</tr>);
		}
		return table;
	}
	createProduct(id) {
		this.state.government.methods
			.create_application(id)
			.send({ from: this.state.account })
			.then(alert("Oo"));
	}
	render() {
		//	let no_of_subsidies = hack();
		// console.log(this.props.government);
		// console.log("1");
		return this.state.subsidiesa.length != 0 ? (
			<div id="table" class="container">
				{this.renderTable()}
			</div>
		) : (
			<h1>loading</h1>
		);
	}
}
export default subsidies;
