import React, { Component } from "react";
import "./App.css";
import { Link } from "react-router-dom";
class Navbar extends Component {
	render() {
		return (
			<nav className="navbar navbar-light fixed-top bg-light flex-md-nowrap shadow bg-transparent">
				<a className="navbar-brand col-sm-3 col-md-2 mr-0" href="/">
					Government of India Subsidies
				</a>

				<ul className="navbar-nav px-3">
					<li className="nav-item text-nowrap d-none d-sm-none d-sm-block">
						<small className="text-white">
							<span id="account">{this.props.account}</span>
						</small>
					</li>
				</ul>
			</nav>
		);
	}
}
export default Navbar;
