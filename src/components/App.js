import React, { Component } from "react";
import logo from "../logo.png";
import "./App.css";
import Web3 from "web3";
import Government from "../abis/Government.json";
import Navbar from "./Navbar";
import home from "./home.js";
import subsidies from "./subsidies.js";
import government from "./government.js";
import application from "./application.js";
import user from "./user.js";
import addSubsidy from "./addSubsidy.js";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

class App extends Component {
	async componentWillMount() {
		await this.loadWeb3();
		await this.loadBlockchainData();
	}

	async loadWeb3() {
		if (window.ethereum) {
			window.web3 = new Web3(window.ethereum);
			await window.ethereum.enable();
		} else if (window.web3) {
			window.web3 = new Web3(window.web3.currentProvider);
		} else {
			window.alert(
				"Non-Ethereum browser detected. You should consider trying MetaMask!"
			);
		}
	}

	async loadBlockchainData() {
		const web3 = window.web3;
		const accounts = await web3.eth.getAccounts();
		//console.log(accounts);
		this.setState({ account: accounts[0] });
		const networkId = await web3.eth.net.getId();
		const networkData = Government.networks[networkId];
		if (networkData) {
			//console.log(Government.abi);
			const government = new web3.eth.Contract(
				Government.abi,
				networkData.address
			);
			this.setState({ government });
			//const no_subsidies = await government.methods.no_subsidies().call();
			//console.log(government)
			//console.log(networkData.address)
			// this.setState({ government });
			//let owner = await government.methods.owner_address().call();
			//	this.setState({ owner });
			// let admins = await government.methods.no_admins().call()
			//let ua = await government.methods.user_addresses().call()
			//let adminss = await government.methods.no_users().call();
			//console.log(adminss);
			// await government.methods.create_user("abdul", "rr","1111").send({from :this.state.account})

			//let sub = await government.methods.no_subsidies().call();
			//console.log(sub);
			//await government.methods
			//.add_admin(this.state.account, "rr", "ee")
			//.send({ from: this.state.account });
			// let admins = await government.methods.no_admins().call();
			// console.log(admins);
			// await government.methods.create_subsidies()

			// let price = 0.5;
			// await government.methods
			// 	.create_subsidies(
			// 		"abdul",
			// 		"rr",
			// 		window.web3.utils.toWei(price.toString(), "Ether")
			// 	)
			// 	.send({ from: this.state.account });

			//this.setState({ owner });
			//console.log(this.state.owner)
		} else {
			window.alert("Marketplace contract not deployed to detected network.");
		}
	}

	constructor(props) {
		super(props);
		this.state = {
			account: "",
			productCount: 0,
			products: [],
			loading: true
		};
	}
	render() {
		return (
			<div>
				<Navbar account={this.state.account} />

				{/* <h1>{this.state.owner}</h1> */}
				<Router>
					<div className="App">
						<Switch>
							<Route path="/" exact component={home}></Route>
							<Route
								path="/subsidies"
								component={subsidies}
								government={this.state.government}
							></Route>
							<Route path="/government" component={government}></Route>
							<Route
								path="/application/:userId"
								component={application}
							></Route>
							<Route path="/user" component={user}></Route>
							<Route path="/addSubsidy" component={addSubsidy}></Route>
						</Switch>
					</div>
				</Router>
			</div>
		);
	}
}

export default App;
