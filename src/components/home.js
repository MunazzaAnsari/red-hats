import "./App.css";
import React, { Component } from "react";
import { Link } from "react-router-dom";
import img from "./okay.svg";

class home extends Component {
	render() {
		return (
			<div className="container-fluid mt-5">
				<div className="row">
					<div class="col-lg-6">
						<div class="jumbotron shadow p-3 mb-5 bg-white rounded">
							<p>
								A <b>subsidy</b> or<b> government incentive </b>is a form of
								financial aid or support extended to an economic sector (or
								institution, business, farmers, or individual) generally with
								the aim of promoting economic and social policy.
							</p>
							<p>
								The subsidised supplements, equipments, food ration etc. are
								filtered through various levels of government bodies before
								reaching the citizen. Due to this the chances of curruption
								increases.
							</p>
							<p>Government subsidies ensures security.</p>
						</div>
						<Link to="/subsidies">
							<button type="button" class="btn btn-primary">
								View Subsidies
							</button>
						</Link>
					</div>
					<div class="col-lg-6">
						<img src={img} />
					</div>
				</div>
			</div>
		);
	}
}
export default home;
