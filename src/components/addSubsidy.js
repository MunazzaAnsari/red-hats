import React, { Component } from "react";
import "./App.css";
import Web3 from "web3";
import Government from "../abis/Government.json";

class user extends Component {
	constructor(props) {
		super(props);
		super();
		this.state = {
			name: "Demo",
			description: "Description",
			ammount: 1
		};
	}

	async componentWillMount() {
		await this.loadBlockchainData();
	}
	handleNameChange(e) {
		this.setState({ name: e.target.value });
	}
	handleAmountChange(e) {
		this.setState({ amount: e.target.value });
	}
	handleDescriptionChange(e) {
		this.setState({ description: e.target.value });
	}

	handleSubmit() {
		// this.state.government.methods
		// 	.create_subsidies(
		// 		this.state.name,
		// 		this.state.description,
		// 		this.state.amount
		// 	)
		// 	.send({ from: this.state.account });
		window.alert("Subsity added");
	}

	async loadBlockchainData() {
		const web3 = window.web3;
		const accounts = await web3.eth.getAccounts();
		//console.log(accounts);
		this.setState({ account: accounts[0] });
		const networkId = await web3.eth.net.getId();
		const networkData = Government.networks[networkId];
		if (networkData) {
			//console.log(Government.abi);
			const government = new web3.eth.Contract(
				Government.abi,
				networkData.address
			);
			this.setState({ government });
			// 			let owner = await government.methods.owner_address().call();
			// 			//console.log(owner);
		} else {
			window.alert("Marketplace contract not deployed to detected network.");
		}
	}

	render() {
		return (
			<div class="container" id="addSubsidy">
				<div class="jumbotron shadow bg-transparent">
					<h4>Please fill the fields:</h4>
					<form>
						<label>
							Name:
							<input
								type="text"
								name="name"
								value={this.state.name}
								onChange={this.handleNameChange}
							/>
						</label>
						<br />
						<label>
							Detail:
							<input
								type="text"
								name="detail"
								value={this.state.description}
								onChange={this.handleDescriptionChange}
							/>
						</label>
						<br />
						<label>
							Amount:
							<input
								type="text"
								name="amount"
								value={this.state.ammount}
								onChange={this.handleAmmountChange}
							/>
						</label>
						<br />
						<button type="button" onClick={this.handleSubmit}>
							submit
						</button>
					</form>
				</div>
			</div>
		);
	}
}
export default user;
