import React, { Component } from "react";
import img from "./okay.svg";
import "./App.css";
import Web3 from "web3";
import Government from "../abis/Government.json";

class Sub {
	constructor(name, details, amount, id) {
		this.name = name;
		this.details = details;
		this.amount = amount;
		this.id = id;
	}
}

class application extends React.Component {
	constructor(props) {
		super(props);
		super();
	}
	async componentWillMount() {
		this.loadWeb3();
		this.loadBlockchainData();
		// this.loadss();
		const params = this.props.match.params.id;
		this.setState({ params });
	}

	async loadWeb3() {
		if (window.ethereum) {
			window.web3 = new Web3(window.ethereum);
			await window.ethereum.enable();
		} else if (window.web3) {
			window.web3 = new Web3(window.web3.currentProvider);
		} else {
			window.alert(
				"Non-Ethereum browser detected. You should consider trying MetaMask!"
			);
		}
	}

	async loadBlockchainData() {
		const web3 = window.web3;
		const accounts = await web3.eth.getAccounts();
		//console.log(accounts);
		this.setState({ account: accounts[0] });
		const networkId = await web3.eth.net.getId();
		const networkData = Government.networks[networkId];
		if (networkData) {
			//console.log(Government.abi);
			const government = new web3.eth.Contract(
				Government.abi,
				networkData.address
			);
			this.setState({ government });
			// 			let owner = await government.methods.owner_address().call();
			// 			//console.log(owner);
			console.log(typeof this.state.params);
			let sub = await government.methods.subsisdies(parseInt(3)).call();

			this.setState({ sub });
		} else {
			window.alert("Marketplace contract not deployed to detected network.");
		}
	}

	ppr() {
		this.state.government.methods
			.create_application(3)
			.send({ from: this.state.account });
	}
	componentDidMount() {
		const params = this.props.match.params.id;
		this.setState({ params });
	}
	render() {
		return this.state.sub ? (
			<div id="application" class="container">
				<div class="row">
					<div class="col-lg-6">
						<div id="app" class="jumbotron shadow p-3 mb-5 bg-white rounded">
							{this.state.sub[0].toString() + this.state.sub[1].toString()}
							<button onClick={this.ppr()} class="btn btn-primary">
								Apply Now
							</button>
						</div>
					</div>
					<div class="col-lg-6">
						<img src={img} />
					</div>
				</div>
			</div>
		) : (
			<h1>loading</h1>
		);
	}
}
export default application;
